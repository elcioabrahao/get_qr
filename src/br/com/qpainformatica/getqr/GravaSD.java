package br.com.qpainformatica.getqr;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class GravaSD extends Activity {
	
	private Button btSalvar;
	private Button btVoltar;
	private EditText arquivo;
	private EditText pasta;
	private String texto;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grava_sd);
		
		btSalvar = (Button)findViewById(R.id.btsalvar);
		btVoltar = (Button)findViewById(R.id.btvoltar);
		arquivo = (EditText)findViewById(R.id.arquivo);
		arquivo.setText("enunciado01.txt");
		pasta = (EditText)findViewById(R.id.pasta);
		pasta.setText("/enunciados");
		
        btVoltar.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){  
    			finish();   			 			
    		}

        }); 

        btSalvar.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){  
    			
    			String root = Environment.getExternalStorageDirectory().toString();
    			File myDir = new File(root + pasta.getText().toString());    
    			myDir.mkdirs();

    			String nomeArquivo = arquivo.getText().toString();
    			if(nomeArquivo.equals("")){
    				nomeArquivo="enunciado01.txt";
    			}
    			File file = new File (myDir, nomeArquivo);
    			if (file.exists ()) {
    				file.delete (); 
    			}
    			byte[] data = new String(texto).getBytes();
    			try {
    			       FileOutputStream out = new FileOutputStream(file);
    			       out.write(data);
    			       out.flush();
    			       out.close();

    			} catch (FileNotFoundException e) {
    				Toast.makeText(GravaSD.this, "Nome de Arquivo ou Diretório inválidos...Arquivo não foi gravado !", Toast.LENGTH_LONG).show();

    			}catch (IOException e) {
    				Toast.makeText(GravaSD.this, "Nome de Arquivo ou Diretório inválidos...Arquivo não foi gravado !", Toast.LENGTH_LONG).show();
    			}
			Toast.makeText(GravaSD.this, "Gravando no SD...", Toast.LENGTH_LONG).show();
			
    		}
    		
        }); 
        
        Intent it = getIntent();
        String t = it.getStringExtra("mensagem");
        if(t != null){
        		texto = t;
        }
        
	}



}
