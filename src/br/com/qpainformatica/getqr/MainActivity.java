package br.com.qpainformatica.getqr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
 
public class MainActivity extends Activity {
    private static final int SPLASH_TIME = 2 * 1000;
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
        new Handler().postDelayed(new Runnable() {
 
            public void run() {
                 
                Intent intent = new Intent(MainActivity.this,
                    MainGetQR.class);
                startActivity(intent);
 
                MainActivity.this.finish();
 
                overridePendingTransition(R.anim.fadein, R.anim.fadeout);
 
            }
             
             
        }, SPLASH_TIME);
         
        new Handler().postDelayed(new Runnable() {
              public void run() {
                     }
                }, SPLASH_TIME);
        } catch(Exception e){}
    }
 
     
    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}