package br.com.qpainformatica.getqr;


import jim.h.common.android.lib.zxing.config.ZXingLibConfig;
import jim.h.common.android.lib.zxing.integrator.IntentIntegrator;
import jim.h.common.android.lib.zxing.integrator.IntentResult;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;


public class MainGetQR extends Activity {
	 private Handler        handler = new Handler();
	    private EditText txtScanResult;
	    private ZXingLibConfig zxingLibConfig;
	    private Button botao1;
	    private Button botaoLimpar;
	    private Button botaoSair;
	    private Button botao2;
	    private Button btAjuda;
	    PopupWindow popUp;
	    LinearLayout layout;
	    TextView tv;
	    LayoutParams params;
	    LinearLayout mainLayout;
	    Button but;
	    boolean click = true;
	    
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_get_qr);

		txtScanResult = (EditText) findViewById(R.id.editText1);
		txtScanResult.setGravity(Gravity.TOP);
		botao1 = (Button) findViewById(R.id.button3);
		botao2 = (Button) findViewById(R.id.button2);
		but = (Button) findViewById(R.id.btajuda);
		
		botaoLimpar = (Button) findViewById(R.id.btLimpar);
		botaoSair = (Button) findViewById(R.id.btSair);
		
        zxingLibConfig = new ZXingLibConfig();
        zxingLibConfig.useFrontLight = true;

        View btnScan = findViewById(R.id.button1);
      
        btnScan.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator.initiateScan(MainGetQR.this, zxingLibConfig);
            }
        });
        
        botao1.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){   			
    			String texto = (String) txtScanResult.getText().toString();   			
    			Intent it = new Intent(v.getContext(), EnviarSms.class);
    			it.putExtra("mensagem", texto);
    			startActivity(it);   			
    		}

    });

        botao2.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){   			
    			String texto = (String) txtScanResult.getText().toString();   			
    			Intent it = new Intent(v.getContext(), GravaSD.class);
    			it.putExtra("mensagem", texto);
    			startActivity(it);   			
    		}

    });        
        
        botaoLimpar.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){   			
    			txtScanResult.setText("");   			 			
    		}

    });
        
        botaoSair.setOnClickListener(new Button.OnClickListener(){      	
    		public void onClick(View v){  
    			finish();   			 			
    		}

    });       
        
        Intent it = getIntent();
        String frase = it.getStringExtra("mensagem");
        if(frase != null){
        
        	txtScanResult.setText(frase);
        }
        
        //popup
        popUp = new PopupWindow(this);
        layout = new LinearLayout(this);
        //mainLayout = new LinearLayout(this);
        mainLayout = (LinearLayout)findViewById(R.id.LinearLayout1);
        tv = new TextView(this);
        
        but.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (click) {
                    popUp.showAtLocation(mainLayout, Gravity.CENTER_VERTICAL, 10, 10);
                    popUp.update(5, 0, 400, 500);
                    click = false;
                } else {
                    popUp.dismiss();
                    click = true;
                }
            }

        });
        params = new LayoutParams(LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
        layout.setOrientation(LinearLayout.VERTICAL);
        tv.setText("GetQR - Utilização:\n\nClique no botão Escanear QR Code, foque " +
        		"a camera no código de barras que você deseja ler a aguarde o processamento. Veja" +
        		" o texto capturado na tela principal.\n\nPara gerar códigos de barras use: http://" +
        		"qpainformatica.com.br/QPAQR/GetCode?qrtext=TEXTO A SER COVERTIDO EM QR CODE." +
        		"\n\nQualquer dúvida entre em contato com elcioabrahao@usp.br.\n\nPara fechar esta janela" +
        		" clique novamente no botão Ajuda.");
        tv.setTextColor(Color.WHITE);
        layout.addView(tv, params);
        popUp.setContentView(layout);
        // popUp.showAtLocation(layout, Gravity.BOTTOM, 10, 10);
        //mainLayout.addView(but, params);
        //setContentView(mainLayout);
            
        
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE: 
           
                IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode,
                        resultCode, data);
                if (scanResult == null) {
                    return;
                }
                final String result = scanResult.getContents();
                if (result != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                        	String texto = (String) txtScanResult.getText().toString();
                        String scape = "";
                        	if(!texto.equals("")){
                        		scape = "\n\n";
                        	}    
                        	txtScanResult.setText(texto+scape+result);
                        }
                    });
                }
                break;
            default:
        }
            
    }
    
    protected void onDestroy(){
    	android.os.Process.killProcess(android.os.Process.myPid());
    	super.onDestroy();

    }
}
