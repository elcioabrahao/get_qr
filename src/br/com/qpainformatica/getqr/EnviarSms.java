package br.com.qpainformatica.getqr;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class EnviarSms extends Activity {
	private static final String CATEGORIA = "qpa";
	EditText mensagemText;
	EditText numeroText;
	Button btEnviar;
	Button btVoltar;
	
	
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		setContentView(R.layout.activity_enviar_sms);

		numeroText   = (EditText) findViewById(R.id.numero);
		mensagemText = (EditText) findViewById(R.id.mensagem);
		
		btEnviar = (Button) findViewById(R.id.btEnviar);
		btEnviar.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {


				String numero   = numeroText.getText().toString();
				String mensagem = mensagemText.getText().toString();

				Log.i(CATEGORIA, "Enviando SMS para ["+numero+"]: " + mensagem);

				Sms sms = new Sms();
				Toast.makeText(EnviarSms.this, "Enviando SMS...", Toast.LENGTH_LONG).show();
				sms.enviarSms(EnviarSms.this, numero,mensagem);
			}});

		btVoltar = (Button) findViewById(R.id.botaovoltar);
		btVoltar.setOnClickListener(new OnClickListener(){
			public void onClick(View view) {

			//String mensagem = mensagemText.getText().toString();
    			//Intent it = new Intent(EnviarSms.this, MainGetQR.class);
    		//	it.putExtra("mensagem", mensagem);
    		//	startActivity(it);
finish();
			}});		
		
        Intent it = getIntent();
        String frase = it.getStringExtra("mensagem");
        if(frase != null){
        	
        	mensagemText.setText(frase);
        }
		
	}
}
